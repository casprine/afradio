let path = require('path'),
  webpack = require('webpack');

const config = {
  output: {
    filename: '[name].js',
  },
  resolve: {
    alias: {
      actions: path.resolve(__dirname, 'app/actions/'),
      pages: path.resolve(__dirname, 'app/components/pages/'),
      containers: path.resolve(__dirname, 'app/components/containers/'),
      presenters: path.resolve(__dirname, 'app/components/presenters/'),
      lib: path.resolve(__dirname, 'app/lib/'),
      components: path.resolve(__dirname, 'app/components/'),
    },
    extensions: ['.js', '.json', '.jsx'],
    modules: [path.resolve(__dirname, 'app'), 'node_modules'],
  },
  module: {
    loaders: [
      {
        test: /\.jsx?/,
        include: path.resolve(`${__dirname}/app/`),
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'es2015-ie', 'react'],
          plugins: ['transform-object-rest-spread'],
        },
      },
    ],
  },
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new webpack.optimize.UglifyJsPlugin({ minimize: true }),
  ],
};

module.exports = config;
