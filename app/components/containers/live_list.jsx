import React, { PureComponent } from 'react';
import MediaCard from 'presenters/media_card.jsx';
import { connect } from 'react-redux';
import { setStream } from 'actions/creators/player';



class LiveList extends PureComponent {
  constructor(props) {
    super(props);
    this.handlePlay = this.handlePlay.bind(this);
  }

  handlePlay(name, image, stationName, stream) {
    this.props.setStream(stream, image, name, stationName);
  }


  renderShowList() {
    if (!this.props.live || this.props.live.length < 1) {
      return <li className="collapse__list-item">no shows airing</li>;
    }

    const slideLists = [];


    for (let i = 0; i < this.props.live.length; i++) {
      const { _id, name, imgUrl, station, streamUrl, } = this.props.live[i];
      const handler = () => {
        this.handlePlay(name, imgUrl, station, streamUrl);
      };
      slideLists.push(<li className="collapse__list-item" key={_id}>
        <MediaCard
          id={_id}
          image={imgUrl}
          title={name}
          subtitle={station}
          actionIconClass="zmdi zmdi-play"
          action={handler}
        />
      </li>);
    }

    return slideLists;
  }


  render() {
    const self = this;

    return (
      <div className="collapse slide">
        <h6 className="collapse__title">on air
        </h6>
        <ul className="collapse__list">
          {this.renderShowList()}
        </ul>
      </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    live: (() => state.appState.live.map(showId => state.appState.shows[showId]))(),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setStream: (stream, image, name, stationName) => {
      dispatch(setStream(stream, image, name, stationName));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LiveList);