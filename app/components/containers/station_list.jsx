import React, { Component } from 'react';
import ItemList from 'containers/item_list.jsx';
import Station from 'presenters/station.jsx';
import { connect } from 'react-redux';
import CardPanelLoader from 'presenters/loaders/card_panel_loader.jsx';

class Stations extends Component {
  populate(stations) {
    const stationComponents = [];

    for (const stationId in stations) {
      const station = stations[stationId];
      stationComponents.push(<Station key={station._id} id={station._id} />);
    }
    return stationComponents;
  }

  render() {
    if (Object.keys(this.props.stations).length === 0) {
      return <CardPanelLoader />;
    }

    const items = this.populate(this.props.stations);
    return <ItemList items={items} />;
  }
}

const mapStateToProps = state => ({
  stations: state.appState.stations,
});

export default connect(
  mapStateToProps,
  null,
)(Stations);
