import { connect } from 'react-redux';
import React, { PureComponent } from 'react';
import { toggleSearch } from 'actions/creators/ui';
import { InstantSearch, Index, Configure, PoweredBy } from 'react-instantsearch/dom';
import SearchBox from 'presenters/search_box';
import ShowResult from 'presenters/show_result';
import StationResult from 'presenters/station_result';
import SearchResultSection from 'presenters/search_result_section';
import { connectStateResults } from 'react-instantsearch/connectors';

class Search extends PureComponent {

    handleFocusIn(e) {
        this.toggleSearch(true);
    }

    handleFocusOut(e) {
        this.toggleSearch(false);
    }

    toggleSearch(toggle) {
        toggle = toggle || !this.props.isOpen
        this.props.toggleSearch(toggle);
    }

    render() {
        const Content = connectStateResults(
            ({ searchState }) => {
                return (searchState.query && searchState.query.trim() !== "") && this.props.isOpen ?
                    (<div className="search-results">
                        <Index indexName="prod_stations">
                            <SearchResultSection title="stations" resultComponent={StationResult} />
                        </Index>
                        <Index indexName="prod_shows">
                            <SearchResultSection title="shows" resultComponent={ShowResult} />
                        </Index>
                        <PoweredBy />
                    </div>)
                    : null;
            }
        );


        const searchClasses = this.props.isOpen ?
            ["search-group", "open"] :
            ["search-group"];

        const instantSearchConfig = {
            Root: "div",
            props: {
                className: searchClasses.join(" "),
                onFocus: this.handleFocusIn.bind(this),
                onClick: event => event.stopPropagation()
            }
        };

        return (
            <InstantSearch
                appId="7VWBYL7OJD"
                apiKey="1cdce56de2ee91f1a94f6ac007f6b027"
                indexName="prod_stations"
                root={instantSearchConfig}>
                <Configure facetFilters={["country:GH"]} />
                <SearchBox />
                <Content />
            </InstantSearch>
        );
    }

}

function mapStateToProps(state) {
    return {
        isOpen: state.ui.search
    };
}

function mapDispatchToProps(dispatch) {
    return {
        toggleSearch(toggle) {
            dispatch(toggleSearch(toggle));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);