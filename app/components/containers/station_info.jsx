import React, { Component } from 'react';
import { connect } from 'react-redux';
import BannerLoader from 'presenters/loaders/banner_loader.jsx';
import { setStream, togglePlay } from 'actions/creators/player';

function renderControlButton(isPlaying, handler) {
  const { d: displayText, i: iconClasses } = isPlaying
    ? { d: 'Pause', i: 'zmdi zmdi-pause' }
    : { d: 'Play', i: 'zmdi zmdi-play' };

  return (
    <button className="button small" type="button" onClick={handler}>
      <i className={iconClasses} />
      {displayText}
    </button>
  );
}

class StationInfo extends Component {
  isCurrentStream() {
    return this.props.station.streamUrl === this.props.player.stream.current;
  }

  isCurrentlyPlaying() {
    return this.isCurrentStream() && this.props.player.playing;
  }

  getImageUrl() {
    return this.props.station.imgUrl
      ? this.props.station.imgUrl
      : 'build/images/station_default.png';
  }

  handlePlayClick() {
    if (this.isCurrentStream()) {
      this.props.togglePlay(!this.props.player.playing);
      return;
    }

    return this.props.setStream(
      this.props.station.streamUrl,
      this.getImageUrl(),
      this.props.station.name,
      this.props.station.frequency,
    );
  }

  render() {
    if (!this.props.station) {
      return <BannerLoader width={`${this.props.width}px`} />;
    }

    const backgroundImage = {
        backgroundImage: `url('${this.getImageUrl()}')`,
      },
      styles = {
        width: `${this.props.width}px`,
      };

    return (
      <div className="station__banner" style={styles}>
        <div className="station__banner-image" style={backgroundImage} />
        <div className="station__banner-text">
          <h4 className="headline">{this.props.station.name}</h4>
          <p className="description"> {this.props.station.description}</p>
          {renderControlButton(this.isCurrentlyPlaying(), this.handlePlayClick.bind(this))}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    station: state.appState.stations[ownProps.stationId],
    player: state.player,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setStream: (stream, image, title, subtitle) => {
      dispatch(setStream(stream, image, title, subtitle));
    },
    togglePlay: (play) => {
      dispatch(togglePlay(play));
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(StationInfo);
