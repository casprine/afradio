import React, { PureComponent } from 'react';
import ItemList from 'containers/item_list';
import Show from 'presenters/show';
import { filterByCategory, mapArrayToObject } from 'lib/util';
import CardPanelLoader from 'presenters/loaders/card_panel_loader.jsx';
import { connect } from 'react-redux';
import { REQUEST_STATES } from 'lib/util';

class ShowList extends PureComponent {
  populate(shows) {
    const showPureComponents = [];

    for (const showId in shows) {
      showPureComponents.push(<Show key={showId} id={showId} />);
    }
    return showPureComponents;
  }

  render() {
    if (this.props.loadingStatus === REQUEST_STATES.PENDING) {
      return <CardPanelLoader />;
    }

    if (this.props.loadingStatus === REQUEST_STATES.COMPLETE) {
      const items = this.populate(this.props.shows);
      return <ItemList items={items} style={this.props.style} />;
    }
  }
}

const mapStateToProps = (state, ownProps) => ({
  shows: (() => {
    if (!ownProps.category && !ownProps.stationId) {
      const shows = {};
      state.appState.featured.forEach(id => (shows[id] = id));
      return shows;
    }

    if (ownProps.stationId) {
      const station = state.appState.stations[ownProps.stationId];

      if (!station) {
        return {};
      }
      const shows = {};
      station.shows.forEach(id => (shows[id] = id));
      return shows;
    }

    return filterByCategory(ownProps.category, state.appState.shows);
  })(),
  loadingStatus: state.request.featured.shows.status,
  error: state.request.featured.shows.error,
});

ShowList.defaultProps = {
  style: {},
};

export default connect(
  mapStateToProps,
  null,
)(ShowList);
