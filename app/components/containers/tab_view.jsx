import React, { PureComponent } from 'react';

export default class TabView extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: 0,
    };
  }

  /**
     * Handles the click event for tab labels
     *
     * @listens TabView#tabview__labels>.label:click
     * @param index
     * @returns {undefined}
     */
  handleLabelClick(index) {
    this.setState({
      activeTab: index,
    });
  }

  /**
     * Renders the appropriate child based on the state of
     * activeTab
     *
     * @returns {string}
     */
  renderActiveTab() {
    const childrenAsArray = React.Children.toArray(this.props.children);
    return childrenAsArray[this.state.activeTab];
  }

  /**
     * Renders the labels for the tabview
     *
     * @returns {string}
     */
  renderLabels() {
    return this.props.labels.map((label, index) => (
      <li
        key={index}
        className={this.state.activeTab === index ? 'active' : ''}
        onClick={this.handleLabelClick.bind(this, index)}>
        {label}
      </li>
    ));
  }

  render() {
    return (
      <div className="tabview">
        <ul className="tabview__labels">
          {this.renderLabels()}
        </ul>
        <div className="tabview__tabs">
          {this.renderActiveTab()}
        </div>
      </div>
    );
  }
}
