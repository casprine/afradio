import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import BannerLoader from 'presenters/loaders/banner_loader';
import { loadShow } from 'actions/creators/app';
import { setStream, togglePlay } from 'actions/creators/player';
import { subscribe, unsubscribe } from 'actions/creators/show';
import { addSubscription, removeSubscription } from 'lib/storage';
import { schedule, unschedule } from 'lib/notificationScheduler';
import { notify } from 'actions/creators/notifications';
import { computeNotificationDelay, notifySuccess } from 'lib/util';

function subscriptionButton(isSubscribed, handler) {
  const
    [displayText, buttonClass, iconClasses] = isSubscribed ?
      ['Unsubscribe', 'subscribed', 'zmdi zmdi-favorite'] :
      ['Subscribe', 'unsubscribed', 'zmdi zmdi-favorite-outline'];

  return (
    <button className={`button small ${buttonClass}`} type="button" onClick={handler} id="subscriber">
      <i className={iconClasses} />
    </button>
  );
}

function renderLastEpisodeButton(isPlaying, handler) {
  const
    { d: displayText, i: iconClasses } = isPlaying ?
      { d: 'Last Episode', i: 'zmdi zmdi-pause' } :
      { d: 'Last Episode', i: 'zmdi zmdi-play' };
  return (
    <button
      className="button small"
      type="button"
      onClick={handler}
    >
      <i className={iconClasses} />{displayText}
    </button>
  );
}

class ShowInfo extends PureComponent {
  constructor(props) {
    super(props);
    this.fetchedShows = [];
  }

  addFetchAttempt(id) {
    this.fetchedShows.push(id);
  }

  fetchAttempted(id) {
    return this.fetchedShows.indexOf(id) > -1;
  }

  isCurrentStream() {
    return this.props.lastEpisode.playbackUrl ===
      this.props.player.stream.current;
  }

  isCurrentlyPlaying() {
    return this.isCurrentStream() && this.props.player.playing;
  }

  /**
     *
     * @returns {String}
     */
  imageUrl() {
    return this.props.show.imgUrl ?
      this.props.show.imgUrl :
      'build/images/show_default.png';
  }

  PureComponentDidMount() {
    this.loadShow();
  }

  PureComponentDidUpdate() {
    this.loadShow();
  }

  loadShow() {
    const { pending, complete } = this.props.requestStatus;
    const fetchAttempted = this.fetchAttempted(this.props.showId);

    if ((!pending && !complete) && !fetchAttempted) {
      this.props.loadShow(this.props.showId);
      this.addFetchAttempt(this.props.showId);
    }
  }

  handleLastEpisodeClick() {
    const { playbackUrl } = this.props.lastEpisode;
    if (this.isCurrentStream()) {
      this.props.togglePlay(!this.props.player.playing);
      return;
    }

    this.props.setStream(
      playbackUrl,
      this.imageUrl(),
      this.props.show.name,
      this.props.show.name,
    );
  }

  handleSubscribeClick() {
    this.props.subscribed ?
      this.unsubscribe() :
      this.subscribe();
  }

  notifySubscription() {
    this.props.notify(notifySuccess(<p className="notification__content">
      Subscribed to <strong>{this.props.show.name}</strong>
    </p>));
  }

  notifyUnsubscription() {
    this.props.notify(notifySuccess(<p className="notification__content">
      Unsubscribed from <strong>{this.props.show.name}</strong>
    </p>));
  }

  /**
     *
     */
  subscribe() {
    let notificationDelay = computeNotificationDelay(this.props.show.schedules),
      {
        name, imgUrl, schedules, _id,
      } = this.props.show;
    this.props.subscribe(this.props.showId);
    const subscriptionMeta = {
      title: name,
      icon: imgUrl,
      schedules,
    };
    addSubscription(this.props.showId, subscriptionMeta);
    if (notificationDelay > -1) {
      schedule({ title: `${name} is live`, icon: imgUrl, id: _id }, notificationDelay);
    }
    this.notifySubscription();
  }

  unsubscribe() {
    this.props.unsubscribe(this.props.showId);
    removeSubscription(this.props.showId);
    unschedule(this.props.showId);
    this.notifyUnsubscription();
  }

  render() {
    if (this.props.requestStatus.pending || !this.props.requestStatus.complete) { return <BannerLoader width={`${this.props.width}px`} error />; }

    const backgroundImage = { backgroundImage: `url('${this.imageUrl()}')` };
    const styles = {
      width: `${this.props.width}px`,
    };
    return (
      <div className="profile__banner" style={styles}>
        <div className="profile__banner-image" style={backgroundImage} />

        <div className="profile__banner-text">
          <h4 className="headline">{this.props.show.name}</h4>
          <p className="description" > {this.props.show.description}</p>
          <div className="below">
            {this.props.lastEpisode ?
              renderLastEpisodeButton(
                this.isCurrentlyPlaying(),
                this.handleLastEpisodeClick.bind(this),
              ) :
              null
            }

            <div className="profile__actions">
              {subscriptionButton(
                this.props.subscribed,
                this.handleSubscribeClick.bind(this),
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const show = state.appState.shows[ownProps.showId];
  const lastEpisode = show && show.lastEpisode ?
    show.lastEpisode :
    null;

  return {
    show,
    lastEpisode,
    subscribed: state.appState.subscriptions.indexOf(ownProps.showId) > -1,
    player: state.player,
    requestStatus: {
      pending: state.request.shows.pending.indexOf(ownProps.showId) > -1,
      failed: !!state.request.shows.errors[ownProps.showId],
      complete: !!show,
    },
  };
}

function mapDispatchToProps(dispatch) {
  return {
    subscribe: (id) => {
      dispatch(subscribe(id));
    },
    unsubscribe: (id) => {
      dispatch(unsubscribe(id));
    },
    togglePlay: (playing) => {
      dispatch(togglePlay(playing));
    },
    setStream: (stream, image, name, stationName) => {
      dispatch(setStream(stream, image, name, stationName));
    },
    loadShow: (id) => {
      dispatch(loadShow(id));
    },
    notify: (notification) => {
      dispatch(notify(notification));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowInfo);
