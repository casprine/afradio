import React,{PureComponent} from 'react';
import {loadShow} from 'actions/creators/app';
import MediaCard from 'presenters/media_card';
import {connect} from 'react-redux';
import {unsubscribe} from 'actions/creators/show';
import {removeSubscription} from 'lib/storage';
import { notify } from 'actions/creators/notifications';
import { notifySuccess } from 'lib/util';

class Subscription extends PureComponent{

    constructor(props){
        super(props);
        this.fetchAttempted = false;
        this.unsubscribeShow = this.unsubscribeShow.bind(this);
    }

    componentDidMount() {
        let {pending,complete} = this.props.requestStatus;

        if ( (!pending && !complete) && !this.fetchAttempted) {
            this.props.loadShow(this.props.id);
            this.fetchAttempted=true;
        }
    }

    render(){
        if (this.props.requestStatus.pending || !this.props.requestStatus.complete){
            return null;
        }

        let {_id,imgUrl,name,station} = this.props.show;
        console.log(_id);

        let handler = () => {this.unsubscribeShow(this.props.id)};
        return (<MediaCard
            id={_id}
            image={imgUrl}
            title={name}
            subtitle={station}
            actionIconClass="zmdi zmdi-block"
            action={handler}
        />);
    }

    unsubscribeShow(id) {
        this.props.unsubscribe(id);
        removeSubscription(id);
        this.notifyUnsubscription();
    }

    notifyUnsubscription(){
        this.props.notify(notifySuccess(
            <p className="notification__content">
                Unsubscribed from <strong>{this.props.show.name}</strong>
            </p>
            )
        );
    }
}

Subscription.defaultPorops = {
    station : { name:"" }
};

function mapStateToProps(state,ownProps){
    let show = state.appState.shows[ownProps.id];
    return {
        show,
        requestStatus :{
            pending : state.request.shows.pending.indexOf(ownProps.id) > -1,
            failed : !!state.request.shows.errors[ownProps.id],
            complete : !!show 
        }
    };
}

function mapDispatchToProps(dispatch){
    return {
        loadShow: id => {
            dispatch(loadShow(id));
        },
        unsubscribe : id => {
            dispatch(unsubscribe(id))
        },
        notify : notification => {
          dispatch(notify(notification))  
        }
    };    
}

export default connect(mapStateToProps,mapDispatchToProps)(Subscription);