import React, { PureComponent } from 'react';
import { togglePlay, setStream, stop, buffering } from 'actions/creators/player';
import plyr from 'plyr';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import { notifyFailure } from 'lib/util';
import { notify } from 'actions/creators/notifications';

class Player extends React.Component {
  setVisibilityClasses() {
    return this.props.open ? 'player' : 'player player--hidden';
  }

  componentDidMount() {
    let
        instance = this.initPlyr({ controls: ['progress']}),
        self = this;
    instance.on('playing',function(){
        if(self.props.buffering){
            self.props.dispatch(buffering(false));
        }
        if(!self.props.playing){
            self.props.dispatch(togglePlay(true));
        }
    })
    .on('pause',function(){
        if(self.props.playing){
            self.props.dispatch(togglePlay(false));
        }
    })
    .on('canplay',function(){
        if(self.props.playing){
            self.plyrInstance.play();
        }
    })
    .on('stalled',function(){
        if(!self.props.buffering){
            self.plyrInstance.pause();
            self.props.dispatch(buffering(true));
        }
    })

    this.plyrInstance = instance;
  }

    /**
     * Initialize a player instance and return the
     * relevant player(the initial object in the array of players)
     *
     * @return object
     */
  initPlyr(options) {
    return plyr.setup(ReactDOM.findDOMNode(this), options)[0];
  }

  /**
     * Determine whether the audio is currently playing
     *
     * @return boolean
     */
  isPlaying() {
    return !this.plyrInstance.getMedia().paused &&
           !this.plyrInstance.getMedia().ended &&
            this.plyrInstance.getMedia().currentTime > 0;
  }

    /**
     * Evaluate whether the currently playing stream and
     * the previous stream are the same
     *
     * @return boolean description
     */
  isSameStream() {
    return this.props.stream.current ===
           this.props.stream.prev;
  }

  /**
     * Sets the audio source for the player
     */
  setAudioSource() {
        this.plyrInstance.source({
          type: 'audio',
          title: 'AFRadio',
          sources: [{
            src: this.props.stream.current, // + ";stream.nsv&type=mp3",
            type: 'audio/mp3',
          }],
        });

        let
            self = this,
            source = this.plyrInstance.getMedia().querySelectorAll('source')[0];

        source.addEventListener("error",function(e){
            self.props.dispatch(notify(notifyFailure(
                <p className="notification__content">
                    The selected audio stream is currently unavailable
                </p>
            )));
            self.stop();
        });

  }

   /**
     * stop the stream and hide the player
     *
     * @return undefined
     */
  stop() {
    this.plyrInstance.pause();
    this.props.dispatch(stop());
  }

   /**
     * Play/Pause the audio
     *
     * @return undefined
     */
  togglePlay() {
        this.props.dispatch(togglePlay(!this.props.playing));
  }

  setPlayState(){
    if(this.props.buffering === true) return;

    if(this.props.playing === true && !this.isPlaying()) {
        this.plyrInstance.play();
    } else if(this.props.playing === false && this.isPlaying()){
        this.plyrInstance.pause();
    }
  }

  render() {
    let iconClasses;

    if (this.props.open) {
      if(this.isSameStream()){
       this.setPlayState();
      }
      else{
        this.setAudioSource();
      }
    }

    if(this.props.buffering === true){
        iconClasses = "zmdi zmdi-refresh-sync zmdi-hc-fw zmdi-hc-spin-reverse"
    }else{
        iconClasses = this.props.playing ?
        'zmdi zmdi-pause zmdi-hc-fw' :
        'zmdi zmdi-play zmdi-hc-fw';
    }

    return (
      <div className={this.setVisibilityClasses()}>
        <audio/>
        <div className="player__panel">
          <div className="player__image">
            <img src={this.props.image} alt="" />
          </div>
          <div className="player__details">
            <div className="player__seek-group" />
            <p>{this.props.title}</p>
            <p>{this.props.subtext}</p>
            {/*  <div className = 'player__seek'> */}
            {/* <div className="player__seek-inner"></div> */}
            {/*   </div> */}
          </div>
        </div>
        <div className="player__controls">
          <span className="zmdi zmdi-replay-10 zmdi-hc-fw" />
          <span
            className={iconClasses}
            onClick={this.togglePlay.bind(this)}
          />
          <span
            className="zmdi zmdi-stop zmdi-hc-fw"
            onClick={this.stop.bind(this)}
          />
          <span className="zmdi zmdi-forward-10 zmdi-hc-fw" />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state){
    return {...state.player};
}

export default connect(mapStateToProps)(Player);