import React, { PureComponent } from 'react';
import StationInfo from 'containers/station_info';
import ShowInfo from 'containers/show_info';
import ShowList from 'containers/show_list';
import PlaybackList from 'containers/playback_list';
// import TabView from 'containers/tab_view';
import rd from 'react-dom';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';

function stationTabLabels() {
  return [<span>Shows</span>];
}

function showTabLabels() {
  return [<span className="af">Previous Episodes</span>];
}

class station extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,
      viewportWidth: window.innerWidth,
    };
    this.calculateDimensions = this.calculateDimensions.bind(this);
  }

  renderStationstation(fixLayout) {
    return (
      <div className="station transition-item">
        <div className="station-detail">
          <StationInfo
            stationId={this.props.id}
            width={this.state.width / 2}
            fixLayout={fixLayout}
          />
        </div>
        <div className="station__body">
          <div className="station__body-section">
            <h4 className="station__body-section-title" style={{ marginTop: '25px' }}>
              Shows
            </h4>
            <ShowList stationId={this.props.id} style={{ paddingTop: '10px' }} />
          </div>
        </div>
      </div>
    );
  }

  renderShowstation(fixLayout) {
    const queryParams = queryString.parse(this.props.location.search),
      selectedPlayback = queryParams.playback || null;
    return (
      <div className="shows">
        <div className="shows-detail">
          <ShowInfo showId={this.props.id} width={this.state.width / 2} fixLayout={fixLayout} />
        </div>
        <div className="shows__body">
          <div className="shows__body-section">
            <h4 className="shows__body-section-title">Previous Episodes</h4>
            <PlaybackList showId={this.props.id} selectedPlayback={selectedPlayback} />
          </div>
        </div>
      </div>
    );
  }

  calculateDimensions() {
    const styles = window.getComputedStyle(this.node, null),
      leftPadding = Number.parseInt(styles.getPropertyValue('padding-left')),
      rightPadding = Number.parseInt(styles.getPropertyValue('padding-right')),
      width = this.node.clientWidth - (rightPadding + leftPadding);
    this.setState({ width, viewportWidth: window.innerWidth });
  }

  componentDidMount() {
    this.node = rd.findDOMNode(this);
    this.calculateDimensions();
    window.addEventListener('resize', this.calculateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.calculateDimensions);
  }

  render() {
    const fixLayout = this.state.viewportWidth > 1600;
    switch (this.props.type) {
      case 'station':
        return this.renderStationstation(fixLayout);
      default:
        return this.renderShowstation(fixLayout);
    }
  }
}

export default withRouter(station);
