import React, { PureComponent } from 'react';
import { fetchPlaybacksForShow } from 'lib/afradio_api';
import Playback from 'presenters/playback';
import { parseDate } from 'lib/util';
import PlaybackListLoader from 'presenters/loaders/playback_list_loader';


export default class PlaybackList extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            playbacks: [],
            loading: false,
            loaded: false,
            error: null,
            showId: props.showId
        }
    }

    componentDidMount() {
        this.loadPlaybacks();
    }

    componentDidUpdate() {
        if (!this.state.loading && (this.state.showId !== this.props.showId)) {
            this.loadPlaybacks();
        }
    }

    /**
     * Load the playbacks for the show
     *
     * @returns {undefined}
     */
    loadPlaybacks() {
        let fetchedFor = this.props.showId;
        this.setState({ loading: true });
        fetchPlaybacksForShow(this.props.showId)
            .end((err, res) => {
                if (err) {
                    const newState = { error: err, loading: false };
                    this.setState(newState);
                    return;
                }

                if (fetchedFor === this.props.showId) {
                    const newState = {
                        loading: false,
                        playbacks: res.body,
                        showId: fetchedFor,
                        error: null
                    };
                    this.setState(newState);
                }
            });
    }

    /**
* Returns an array of Playback components genrerated from items
*
* @param {object[]} items
* @returns {Array|Show.populatePlaybacks.shows}
*/
    renderPlaybacks(playbacks) {
        return playbacks.map((item) => {
            const date = parseDate(item.sessionDate);
            return <Playback
                source={item.playbackUrl}
                key={item._id}
                id={item._id}
                data={item}
                autoPlay={this.props.selectedPlayback == item._id}
                showId={this.props.showId}
                title={date.toDateString()} />
        });
    }

    render() {

        let listClasses = "playback-list"
        if (this.state.playbacks.length < 1) {
            if (this.state.loading) {
                return <PlaybackListLoader />
            }

            if (this.state.error) {
                return (
                    <div className='playback-list playback-list--error'>
                        <p>An error ocurred</p>
                        <button
                            className="button small"
                            onClick={this.loadPlaybacks.bind(this)}>Retry</button>
                    </div>
                );
            }

            return (
                <div className='playback-list playback-list--empty'>
                    <p>No playbacks are available for this show currently</p>
                </div>
            );
        }


        return (
            <div className='playback-list'>
                {this.renderPlaybacks(
                    this.state.playbacks,
                    this.props.showId)}
            </div>
        )
    }

}