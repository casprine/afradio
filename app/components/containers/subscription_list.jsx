import React, { Component } from 'react';
import Subscription from 'containers/subscription';
import { connect } from 'react-redux';

class SubscriptionList extends Component {
    renderShowList() {
        if (!this.props.subs || this.props.subs.length < 1) {
            return <li className="collapse__list-item">No Subscriptions</li>;
        }
        const SlideLists = [];

        for (let i = 0; i < this.props.subs.length; i++) {
            SlideLists.push(<li className="collapse__list-item" key={this.props.subs[i]}>
                <Subscription id={this.props.subs[i]} />
            </li>, );
        }

        return SlideLists;
    }


    render() {
        const self = this;

        return (
            <div className="collapse slide">
                <h6 className="collapse__title">Subscriptions</h6>

                <ul className="collapse__list">
                    {this.renderShowList()}
                </ul>
            </div>

        );
    }
}

function mapStateToProps(state) {
    return {
        subs: state.appState.subscriptions,
    };
}

export default connect(mapStateToProps)(SubscriptionList);
