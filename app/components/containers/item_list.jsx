import React from 'react';

export default function ItemList({ items, style }) {
  return (
    <div className="card-panel transition-item" style={style}>
      {items}
    </div>
  );
}
