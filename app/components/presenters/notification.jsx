import React, {PureComponent} from 'react';
import {notify,removeNotification} from 'actions/creators/notifications';
import {connect} from 'react-redux';

class Notification extends PureComponent{
    
    componentDidMount(){
        setTimeout(() => {
            this.props.removeNotification(this.props.id);
        },this.props.delay);
    }
      
    render(){
        const classNames = `notification notification--${this.props.type}`;
        return <div className={classNames}>{this.props.message}</div>
    }
}
 
function mapDispatchToProps(dispatch){
    return {
        removeNotification : id => {
            dispatch(removeNotification(id))
        }
    };
}

export default connect(null,mapDispatchToProps)(Notification);