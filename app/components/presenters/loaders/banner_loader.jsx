import React from 'react';

export default function BannerLoader(props) {
  const styles = {
    width: props.width,
  };

  const reloadButton = props.error ? (
    <button className="profile__banner-reload">
      <i className="zmdi zmdi-refresh" />
    </button>
  ) : null;

  return (
    <div className="profile__banner loader" style={styles}>
      <div className="profile__banner-image image-loader" />
      <div className="profile__banner-text">
        <div>
          <h4 className="text-loader text-loader--short" />
          <div className="text-loader" />
          <div className="text-loader" />
        </div>
      </div>
      {reloadButton}
    </div>
  );
}
