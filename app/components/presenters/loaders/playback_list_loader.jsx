import React from 'react';

export default function PlaybackListLoader(props) {
  return (
    <div className="playback__list loader">
      <div className="playback playback--loader text-loader" />
      <div className="playback playback--loader text-loader" />
      <div className="playback playback--loader text-loader" />
      <div className="playback playback--loader text-loader" />
      <div className="playback playback--loader text-loader" />
      <div className="playback playback--loader text-loader" />
      <div className="playback playback--loader text-loader" />
    </div>
  );
}
