import React from 'react';

function loaders() {
  const loaders = [];
  for (let i = 0; i < 8; i++) {
    loaders.push(<div className="card show" key={i}>
      <div className="card__container">
        <div className="card__image image-loader" />
        <div className="card__details">
          <div className="text-loader text-loader--thin" style={{ marginTop: '15px' }} />
          <div className="text-loader text-loader--thin" />
        </div>
      </div>
                 </div>);
  }
  return loaders;
}

export default function CardPanelLoader(props) {
  return <div className="card-panel">{loaders()}</div>;
}
