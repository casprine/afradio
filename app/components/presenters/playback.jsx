import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setStream, togglePlay } from 'actions/creators/player';
import { share, constructShareUrl, SHARING_PLATFORMS } from 'lib/share';

class Playback extends Component {
  componentDidMount() {
    if (this.props.autoPlay) {
      this.handleEpisodePlay();
    }
  }
  /**
   *
   * @param {type} episode
   * @returns {undefined}
   */
  handleEpisodePlay() {
    if (!this.props.isCurrentStream) {
      this.props.setStream(
        this.props.source,
        this.props.show.imgUrl || this.defaultImage(),
        this.props.show.name,
        this.props.show.station,
      );

      return;
    }

    this.props.togglePlay(!this.props.isPlaying);
  }

  handleEpisodeShared(platform) {
    const shareHandler = function (e) {
      e.stopPropagation();
      console.log(constructShareUrl(window.location.href, { playback: this.props.id }));
      share(platform, {
        url: constructShareUrl(window.location.href, { playback: this.props.id }),
        title: encodeURIComponent(`${this.props.show.name} (${this.props.title})`),
      });
    };

    return shareHandler.bind(this);
  }

  render() {
    if (!this.props.show) {
      return null;
    }

    const iconClasses =
      this.props.isCurrentStream && this.props.isPlaying ? 'zmdi zmdi-pause' : 'zmdi zmdi-play';

    return (
      <div className="playback" onClick={this.handleEpisodePlay.bind(this)}>
        <div className="playback__action">
          <i className={iconClasses} />
        </div>
        <div className="playback__details">
          <p>{this.props.title}</p>
          <ul className="share-list inline-list right">
            <li className="fb-share" onClick={this.handleEpisodeShared(SHARING_PLATFORMS.FACEBOOK)}>
              <i className="zmdi zmdi-facebook-box" />
            </li>
            <li className="tw-share" onClick={this.handleEpisodeShared(SHARING_PLATFORMS.TWITTER)}>
              <i className="zmdi zmdi-twitter-box" />
            </li>
            <li className="ln-share" onClick={this.handleEpisodeShared(SHARING_PLATFORMS.LINKEDIN)}>
              <i className="zmdi zmdi-linkedin-box" />
            </li>
            <li
              className="gp-share"
              onClick={this.handleEpisodeShared(SHARING_PLATFORMS.GOOGLEPLUS)}
            >
              <i className="zmdi zmdi-google-plus-box" />
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    show: state.appState.shows[ownProps.showId],
    isCurrentStream: state.player.stream.current === ownProps.source,
    isPlaying: state.player.playing,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    togglePlay: (playing) => {
      dispatch(togglePlay(playing));
    },
    setStream: (stream, image, name, stationName) => {
      dispatch(setStream(stream, image, name, stationName));
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Playback);
