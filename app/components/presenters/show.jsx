import React, { PureComponent } from 'react';
import { NavLink as Link, withRouter } from 'react-router-dom';
import { setStream } from 'actions/creators/player';
import { subscribe, unsubscribe } from 'actions/creators/show';
import { loadShow } from 'actions/creators/app';
import { addSubscription, removeSubscription } from 'lib/storage';
import { computeNotificationDelay, notifySuccess } from 'lib/util';
import { schedule, unschedule } from 'lib/notificationScheduler';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { notify } from 'actions/creators/notifications';


function renderSubscriptionButton(isSubscribed, handler) {

    const [buttonClasses, iconClasses] = isSubscribed ?
        ['card__action__unsubscribe', 'zmdi zmdi-favorite zmdi-hc-fw'] :
        ['card__action__subscribe', 'zmdi zmdi-favorite-outline zmdi-hc-fw'];

    return (
        <span
            className={`card__action card__action--small ${buttonClasses}`}
            onClick={handler}>
            <span className="card__action-icon">
                <i className={iconClasses} />
            </span>
        </span>
    );
}

class Show extends PureComponent {


    componentDidMount() {
        if (!!!this.props.name) {
            this.props.loadShow(this.props.id);
        }
    }
    /**
       * Returns the url to the default image for this
       * component
       *
       * @returns {string}
       */
    defaultImage() {
        return 'build/images/show_default.png';
    }

    /*
       * Transitions to the individual page for the
       * show
       *
       * @returns {undefined}
       */
    linkToPage() {
        this.props.navigateTo(`/shows/${this.props.id}`);
    }

    /**
     * Renders the play last episode button
     *
     * @returns {string} the markup for the button
     */
    renderLastEpisodeButton() {
        if (this.props.lastEpisode) {
            return (
                <span
                    className="card__action card__action--adjust-right"
                    onClick={this.playLastEpisode.bind(this)}>
                    <span className="card__action-icon">
                        <i className="zmdi zmdi-play zmdi-hc-fw" />
                    </span>
                </span>
            );
        }
    }

    /**
     * Handler for the click event of the
     * play action
     *
     * @listens Show#card__action>play:click
     * @param {Event}
     */
    playLastEpisode(e) {
        e.stopPropagation();
        this.props.setStream(
            this.props.lastEpisode.playbackUrl,
            this.props.image || this.defaultImage(),
            this.props.name,
            this.props.station,
        );
    }

    /**
     * Handler for the click event of the
     * subscribe action
     *
     * @listens Show#card__action>star:click
     * @param {Event}
     */
    subscribe() {
        let
            { name, image, schedules, id } = this.props,
            notificationDelay = computeNotificationDelay(schedules);
        this.props.subscribe(id);
        let subscriptionMeta = {
            title: name,
            icon: image,
            schedules: schedules
        };
        addSubscription(id, subscriptionMeta);
        if (notificationDelay > -1) {
            schedule({ title: `${name} is live`, icon: image, id: id }, notificationDelay);
        }
        this.notifySubscription();
    }

    notifySubscription() {
        this.props.notify(notifySuccess(
            <p className="notification__content">
                Subscribed to <strong>{this.props.name}</strong>
            </p>
        )
        );
    }

    notifyUnsubscription() {
        this.props.notify(notifySuccess(
            <p className="notification__content">
                Unsubscribed from <strong>{this.props.name}</strong>
            </p>
        )
        );
    }

    handleSubscriptionClick(e) {
        e.stopPropagation();
        this.props.subscribed ?
            this.unsubscribe() :
            this.subscribe();
    }

    /**
       * Handler for the click event of the
       * unsubscribe action
       *
       * @listens Show#card__action>block:click
       * @param {Event}
       */
    unsubscribe(e) {
        this.props.unsubscribe(this.props.id);
        removeSubscription(this.props.id);
        unschedule(this.props.id);
        this.notifyUnsubscription();
    }

    render() {
        if (!this.props.name) {
            return null;
        }

        return (
            <div className="card show">
                <div className="card__container">
                    <div className="card__image" style={{ backgroundImage: `url('${this.props.image || this.defaultImage()}')` }} onClick={this.linkToPage.bind(this)} >
                        {/* <div className="card__image" style ={{backgroundImage: `url('${this.defaultImage()}')`}} onClick ={this.linkToPage.bind(this)}>  */}
                        <div className="card__actions">
                            <div className="card__menu">
                                {renderSubscriptionButton(
                                    this.props.subscribed,
                                    this.handleSubscriptionClick.bind(this))
                                }
                                {this.renderLastEpisodeButton()}
                            </div>
                        </div>
                    </div>
                    <div className="card__details">
                        <Link to={`/shows/${this.props.id}`} >
                            <p className="card__title">{this.props.name}</p>
                        </Link>
                        <p className="card__station">{this.props.station}</p>
                        {this.props.genres.map((each , index) => (<span className="card__tag" key={index}> {each}  </span>))}
                    </div>
                </div>
            </div>
        );
    }
}

function nullProps() {
    return {
        subscribed: null,
        lastEpisode: null,
        station: null,
        image: null,
        name: null
    };
}

function mapStateToProps(state, ownProps) {
    const show = state.appState.shows[ownProps.id];
    if (!show) {
        return nullProps();
    }

    let lastEpisode = null;
    if (show.lastEpisode) {
        lastEpisode = show.lastEpisode;
    }
    return {
        subscribed: (state.appState.subscriptions.indexOf(ownProps.id) > -1),
        lastEpisode: lastEpisode,
        station: show.station,
        image: show.imgUrl,
        name: show.name,
        genres: show.genres,
        schedules: show.schedules
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setStream: (stream, image, title, subtitle) => {
            dispatch(setStream(stream, image, title, subtitle));
        },
        subscribe: id => {
            dispatch(subscribe(id));
        },
        unsubscribe: id => {
            dispatch(unsubscribe(id));
        },
        loadShow: id => {
            dispatch(loadShow(id));
        },
        notify: notification => {
            dispatch(notify(notification))
        },
        navigateTo: location => {
            dispatch(push(location));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Show);