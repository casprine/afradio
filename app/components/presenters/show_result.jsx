import React,{Component} from 'react';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { toggleSearch } from 'actions/creators/ui';

class ShowResult extends Component{
    
    handleClick(){
        this.props.navigateTo(`/shows/${this.props.hit._id}`);
         this.props.toggleSearch(false);
    }
    
    render(){
        const 
            hit = this.props.hit,
            background = {
                backgroundImage:`url(${hit.imgUrl})`
            }
       return( <div className="search-result search-result--show" onClick={this.handleClick.bind(this)}>
           <div className="search-result__thumbnail" style={background} ></div>
           <div className="search-result__details">
             <p className="search-result__title">
            {hit.name}
            </p>
                <p className="search-result__subtitle"></p>           
            </div>
        </div>);
    }    
}

function mapDispatchToProps(dispatch){
    return {
        navigateTo : location => {
            dispatch(push(location));
        },
        toggleSearch(toggle){
            dispatch(toggleSearch(toggle));
        } 
    };
}

export default connect(null,mapDispatchToProps)(ShowResult);