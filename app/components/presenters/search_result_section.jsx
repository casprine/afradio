import React, { Component } from 'react';
import { connectStateResults } from 'react-instantsearch/connectors';

class SearchResultSection extends Component {
  renderContent() {
    if (this.props.searching === true) {
      return <div>searching</div>;
    }
    const results = this.props.searchResults;
    return results && results.nbHits !== 0 ? (
      <div className="search-results__list">
        {results.hits.map(hit => <this.props.resultComponent key={hit.objectID} hit={hit} />)}
      </div>
    ) : (
      <div>no search results</div>
    );
  }

  render() {
    return (
      <div className="search-results__section">
        <h5 className="search-results__section-title">{this.props.title}</h5>
        {this.renderContent()}
      </div>
    );
  }
}

export default connectStateResults(SearchResultSection);
