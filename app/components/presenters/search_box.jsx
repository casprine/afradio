import { connect } from 'react-redux';
import React, { Component } from 'react';
import { toggleSearch } from 'actions/creators/ui';
import { connectSearchBox } from 'react-instantsearch/connectors';
import debounce from 'lodash.debounce';

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.performSearch = debounce((e) => {
      // if(e.target.value.trim() === "") return;
      // console.log(e.target.value);
      this.props.refine(e.target.value);
    }, 500);

    this.toggleSearch = this.toggleSearch.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleClear = this.handleClear.bind(this);
  }

  handleSearch(e) {
    e.persist();
    this.performSearch(e);
  }

  handleClear() {
    this.textInput.value = '';
    this.textInput.focus();
  }

  toggleSearch(toggle) {
    toggle = toggle || !this.props.isOpen;
    this.props.toggleSearch(toggle);
  }

  render() {
    return (
      <div style={{ width: '100%' }}>
        <input
          type="text"
          placeholder="Search by radio shows, radio stations, frequency, hosts and genres"
          onChange={this.handleSearch}
          ref={(input) => {
            this.textInput = input;
          }}
        />
        <button onClick={() => this.toggleSearch(false)} className="search-back hide-for-large-up">
          <i className="zmdi zmdi-hc-fw zmdi-arrow-left" />
        </button>
        <button className="search-back show-for-large-up">
          <i className="zmdi zmdi-hc-fw zmdi-search" />
        </button>
        <button onClick={this.handleClear} className="search-clear">
          <i className="zmdi zmdi-hc-fw zmdi-close" />
        </button>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isOpen: state.ui.search,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    toggleSearch(toggle) {
      dispatch(toggleSearch(toggle));
    },
  };
}

export default connectSearchBox(connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchBox));
