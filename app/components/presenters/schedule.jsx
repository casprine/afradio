import React from 'react';
import { minutesToTimeString, afDayAsString } from 'lib/util';

function ScheduleItem(props) {
  return (
    <div className="schedule__item">
      <div className="schedule__item__day">
        <div className="schedule__item__day-inner">
          <p>{ props.day }</p>
        </div>
      </div>
      <div className="schedule__item__time">
        <div className="schedule__item__time-inner">
          <p> {props.start} </p>
          <p>-</p>
          <p>  {props.end}</p>
        </div>
      </div>
    </div>
  );
}

export default class Schedule extends React.Component {
  /**
     *
     */
  renderScheduleItems() {
    return this.props.schedule.map(item => (<ScheduleItem
      day={afDayAsString(item.day)}
      start={minutesToTimeString(item.start)}
      end={minutesToTimeString(item.start + item.duration)}
    />
    ));
  }

  render() {
    return (
      <div className="schedule">
        <div className="schedule__title">
          <i className="zmdi zmdi-time" />
        </div>
        {this.renderScheduleItems()}
      </div>
    );
  }
}
