import React, { PureComponent } from 'react';
import { NavLink as Link, withRouter } from 'react-router-dom';
import { setStream } from 'actions/creators/player';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

class Station extends PureComponent {
  constructor(props) {
    super(props);
    this.countries = new Map([['NG', 'Nigeria'], ['KE', 'Kenya'], ['GH', 'Ghana']]);
  }

  /**
     * Returns the default image for this component
     *
     * @returns {string}
     */
  defaultImage() {
    return 'build/images/station_default.png';
  }

  /**
     * Transitions the router to the individual page for the
     * Station
     *
     * @returns {undefined}
     */
  linkToPage() {
    this.props.navigateTo(`/stations/${this.props.id}`);
  }

   /**
     * Handler for the click event on the play action button
     *
     * @listens Station#card__action-icon>play:click
     * @param {Event}
     */
  playStream(e) {
    e.stopPropagation();
    this.props.setStream(
      this.props.streamUrl,
      this.props.imgUrl || this.defaultImage(),
      this.props.name,
      this.props.frequency,
    );
  }

  render() {
    return (
      <div className="card show">
        <div className="card__container">
         <div className="card__image" style={{ backgroundImage: `url('${this.props.imgUrl || this.defaultImage()}')` }} onClick={this.linkToPage.bind(this)}> 
            {/* <div className="card__image" style = {{backgroundImage: `url('${this.defaultImage()}')` }} onClick = {this.linkToPage.bind(thi s)}>  */}
            <div className="card__actions">
              <div className="card__menu">
                <span className="card__action card__action--adjust-right" >
                  <span className="card__action-icon" onClick={this.playStream.bind(this)}>
                    <i className="zmdi zmdi-play zmdi-hc-fw" />
                  </span>
                </span>
              </div>
            </div>
          </div>
          <div className="card__details">
            <Link to={`/stations/${this.props.id}`} >
              <p className="card__title dark-text">{this.props.name}</p>
            </Link>
            <p className="card__station">{this.countries.get(this.props.country)}</p>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state,ownProps){
    const station = state.appState.stations[ownProps.id];
    return {
        name:station.name,
        country:station.country,
        streamUrl:station.streamUrl,
        imgUrl:station.imgUrl,
        frequency:station.frequency
    }
}

function mapDispatchToProps(dispatch){
    return {
        setStream: (stream,image,title,subtitle) => {
            dispatch(setStream(stream,image,title,subtitle));
        },
        navigateTo : location => {
            dispatch(push(location));
        }
    };
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Station));
