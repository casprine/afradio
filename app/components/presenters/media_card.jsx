import React, { Component } from 'react';
import { NavLink as Link, withRouter } from 'react-router-dom';

class MediaCard extends Component {
  render() {
    return (
      <div className="media-card">
        <div className="media-card__image">
          <div className="media-card__action" onClick={this.props.action}>
            <i className={this.props.actionIconClass} />
          </div>
          <img src={this.props.image} alt="" />
        </div>
        <div className="media-card__details">
          <Link to={`/shows/${this.props.id}`}>
            <p className="media-card__headline">{this.props.title}</p>
          </Link>
          <p>{this.props.subtitle}</p>
        </div>
      </div>
    );
  }
}

export default MediaCard;
