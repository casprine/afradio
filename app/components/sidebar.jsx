import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { toggleLiveMenu } from 'actions/creators/ui';
import { updateLiveShows as updateLive } from 'actions/creators/app';
import { connect } from 'react-redux';
import isEqual from 'lodash.isequal';
import LiveList from 'containers/live_list';
import SubscriptionList from 'containers/subscription_list';

function sidebarClasses(isOpen) {
  const base = 'sidebar sidebar--secondary sidebar--custom-scroll',
    open = isOpen ? ' open' : '';

  return `${base}${open}`;
}

class Sidebar extends Component {
  shouldComponentUpdate(nextProps) {
    return this.props.isOpen !== nextProps.isOpen;
  }
  _;

  render() {
    return (
      <div className={sidebarClasses(this.props.isOpen)}>
        <span className="sidebar__closer" onClick={this.props.close}>
          <i className="zmdi zmdi-close" />
        </span>
        <Scrollbars autoHide renderView={props => <div {...props} className="scroller" />}>
          <LiveList />
          <div className="seperator" />
          <SubscriptionList />

          <div className="seperator" />

          <div className="heading"> Go Mobile </div>
          <div className="downloads">
            <a
              href="https://itunes.apple.com/gh/app/afradio-ghana-radio/id1286903368?mt=8"
              target="_blank"
            >
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/2000px-Download_on_the_App_Store_Badge.svg.png"
                alt="download from"
              />
            </a>

            <a href="https://play.google.com/store/apps/details?id=xyz.chanl.radio" target="_blank">
              <img
                src="https://www.talkhomeapp.com/images/apps/download_app_google_play.svg"
                alt=""
              />
            </a>
          </div>

          <div className="seperator" />

          <div className="ads">
            <a href="/">For Broadcasters</a>
            <a href="/">For Advertisers</a>
          </div>
          <div className="seperator" />

          <div className="terms">
            <a href="/"> terms & conditions </a>
            <a href="/"> privacy policy </a>
            <a href="/"> Cookie policy </a>
          </div>
        </Scrollbars>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isOpen: state.ui.liveMenu,
  };
}

const mapDispatchToProps = dispatch => ({
  close: () => {
    dispatch(toggleLiveMenu(false));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Sidebar);
