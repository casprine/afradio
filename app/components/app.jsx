import React, { Component } from 'react';
import Header from 'components/header';
import Nav from 'components/nav';
import Sidebar from 'components/sidebar';
import Player from 'containers/player';
import { loadStations, loadShows } from 'actions/creators/app';
import { toggleLiveMenu, toggleNavMenu, toggleSearch } from 'actions/creators/ui';
import { connect } from 'react-redux';
import {subRoutes} from 'routes';
import Notifier from 'components/notifier';


class App extends Component {
  
    componentDidMount() {
        this.props.dispatch(loadStations());
        this.props.dispatch(loadShows());
    }

  /**
     * Returns a class string for the main wrapper element
     * based on state and active components
     *
     * @returns {String}
     */
    setDisplayClasses() {
        return this.props.isPlayerOpen ?
        'content playing' : 'content';
    }
  
    dismissMenus(){
        if (this.props.isNavOpen) this.props.toggleNav(false);
        if (this.props.isSearchOpen) this.props.toggleSearch(false);
    }

  render() {
    return (
      <div className={this.setDisplayClasses()} onClick={this.dismissMenus.bind(this)}>
        <Notifier />
        <Header />
        <Nav />
        <Sidebar />
        <div className="main">
          <div className="main__content">
            {subRoutes(this.props.match.url)}
          </div>
        </div>
        <Player />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isPlayerOpen: state.player.open,
  isNavOpen:state.ui.nav,
  isSearchOpen:state.ui.search
});

const mapDispatchToProps = dispatch => ({
    dispatch,
    toggleNav(toggle){
        dispatch(toggleNavMenu(toggle));
    },
    toggleSearch(toggle){
        dispatch(toggleSearch(toggle));
    }
});

export default connect(mapStateToProps,mapDispatchToProps)(App);