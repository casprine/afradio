import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import Notification from 'presenters/notification';
import { CSSTransitionGroup } from 'react-transition-group';





class Notifier extends PureComponent{
    
    renderNotifications(){
        let 
            notifications = this.props.notifications, 
            i = (notifications.length - 1),
            renderedNotifications = [];
        for(i;i > -1; i--){
            renderedNotifications.push(
                <Notification key={notifications[i].id} {...notifications[i]}/>
            ); 
        }
        return renderedNotifications;
    }
    
    render(){
        return(
        <div className="notification-panel">
           <CSSTransitionGroup
          transitionName="example"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}>
            {this.renderNotifications()}
            </CSSTransitionGroup>
        </div>
        );
    }    
}

function mapStateToProps(state){
    return{
        notifications : state.notifications
    };
};

export default connect(mapStateToProps,null)(Notifier);