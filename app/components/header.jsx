import React, { Component } from 'react';
import Search from './containers/search';
import { toggleLiveMenu, toggleNavMenu, toggleSearch } from 'actions/creators/ui';
import { connect } from 'react-redux';

class Header extends Component {
  toggleLiveMenu() {
    this.props.toggleLive(!this.props.ui.liveMenu);
  }

  toggleNavMenu() {
    this.props.toggleNav(!this.props.ui.nav);
  }

  toggleSearch() {
    this.props.toggleSearch(!this.props.ui.search);
  }

  render() {
    return (
      <div className="header">
        <div className="logo">
          <a href="/">
            <img className="hide-for-small-only" src="build/images/af_logo_dark.png" alt="" />
          </a>
          <a href="/">
            <img className="show-for-small-only" src="build/images/af_logo_small.png" alt="" />
          </a>
        </div>
        <Search />
        <div className="header__controls hide-for-large-up" onClick={e => e.stopPropagation()}>
          <button className="button small search" onClick={this.toggleSearch.bind(this)}>
            <i className="zmdi zmdi-search zmdi-hc-fw" />
          </button>
          <button className="button small radio" onClick={this.toggleLiveMenu.bind(this)}>
            <i className="zmdi zmdi-radio zmdi-hc-fw" />
          </button>
          <button
            className="button small show-for-small-only menu"
            onClick={this.toggleNavMenu.bind(this)}
          >
            <i
              className={
                this.props.ui.nav ? 'zmdi zmdi-close zmdi-hc-fw' : 'zmdi zmdi-menu zmdi-hc-fw'
              }
            />
          </button>
        </div>
        <div className="nav-ads">
          <a href="/">Broadcasters</a>
          <a href="/">Advertisers </a>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ui: state.ui,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    toggleLive(toggle) {
      dispatch(toggleLiveMenu(toggle));
    },
    toggleNav(toggle) {
      dispatch(toggleNavMenu(toggle));
    },
    toggleSearch(toggle) {
      dispatch(toggleSearch(toggle));
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
