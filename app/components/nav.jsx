import React, { Component } from 'react';
import { NavLink as Link } from 'react-router-dom';
import { fetchCategories } from 'lib/afradio_api';
import { toggleNavMenu } from 'actions/creators/ui';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

class Nav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: undefined,
    };
  }

  componentDidMount() {
    fetchCategories().end((err, res) => {
      if (!err) {
        this.setState({ categories: res.body });
      }
    });
  }
  
//  shouldComponentUpdate(){
//      return !!!this.state.categories;
//  }

  renderCategories() {
    if (this.state.categories) {
      return this.state.categories.map((category, index) => (
        <li key={index}>
          <Link to={`/categories/${category.key}`} activeClassName="active">
            {category.name}
          </Link>
        </li>));
    }
    return (<li>loading categories..</li>);
  }

  setClasses() {
    const base = 'sidebar';
    const open = this.props.ui.nav ? ' open' : '';
    return `${base}${open}`;
  }

  render() {
    return (
      <div className={this.setClasses()}>
        <div className="nav">
          <ul className="nav_section">
            <li>Browse</li>
            <li><Link to="/shows" activeClassName="active">Shows</Link></li>
            <li><Link to="/stations" activeClassName="active">Stations</Link></li>
          </ul>
          <ul className="nav_section">
            <li>Categories</li>
            {this.renderCategories()}
          </ul>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state){
    return {
        ui : state.ui
    }
}

export default withRouter(connect(mapStateToProps,null)(Nav));