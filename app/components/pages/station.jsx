import React, { Component } from 'react';
import Profile from 'containers/profile';

export default class Station extends Component {
  render() {
    return <Profile type="station" id={this.props.match.params.id} />;
  }
}
