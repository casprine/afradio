import React, { PureComponent } from 'react';
import Profile from 'containers/profile';

export default class Show extends PureComponent {
  render() {
    return <Profile id={this.props.match.params.id} type="show" />;
  }
}
