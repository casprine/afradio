import React, { PureComponent } from 'react';
import ShowList from 'containers/show_list';

export default class Shows extends PureComponent {
  render() {
    return <ShowList />;
  }
}
