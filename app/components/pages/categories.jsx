import React, { Component } from 'react';
import ShowList from 'containers/show_list';

export default class Categories extends Component{
    render(){    
    return (
        <ShowList category={this.props.match.params.key} />
    ); 
    }
}
