import React, { Component } from 'react';
import StationList from 'containers/station_list.jsx';

export default class Stations extends Component{
    
    render(){   
        return (
            <StationList/>
        );  
    }
}
