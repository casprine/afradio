import React from 'react';
import { Route, Switch } from 'react-router-dom';
import App from 'components/app';
import Categories from 'pages/categories';
import Stations from 'pages/stations';
import Shows from 'pages/shows';
import Show from 'pages/show';
import Station from 'pages/station';

export default (<Route component={App} path="/" />);

export const subRoutes = baseUrl => (
  <Switch>
    <Route exact path={`${baseUrl}`} component={Shows} />
    <Route exact path={`${baseUrl}stations`} component={Stations} />
    <Route exact path={`${baseUrl}stations/:id`} component={Station} />
    <Route exact path={`${baseUrl}shows`} component={Shows} />
    <Route exact path={`${baseUrl}shows/:id`} component={Show} />
    <Route exact path={`${baseUrl}categories/:key`} component={Categories} />
  </Switch>
);
