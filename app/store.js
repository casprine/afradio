import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from 'reducers';
import createHistory from 'history/createHashHistory';
import { routerReducer, routerMiddleware, push } from 'react-router-redux'

const history = createHistory();

const rMiddleware = routerMiddleware(history);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(reducer, composeEnhancers(applyMiddleware(thunk,rMiddleware)));