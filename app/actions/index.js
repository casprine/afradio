// app related actions
export const SET_SHOWS = 'SET_SHOWS';
export const SET_STATIONS = 'SET_STATIONS';
export const OPEN_SEARCH = 'OPEN_SEARCH';
export const ADD_SHOW = 'ADD_SHOW';
export const CLOSE_SEARCH = 'CLOSE_SEARCH';
export const UPDATE_LIVE_SHOWS = 'REFRESH_LIVE_SHOWS';
export const UPDATE_FEATURED_SHOWS = 'UPDATE_FEATURED_SHOWS';
export const INITIALIZE_SHOW_DATA = 'INITIALIZE_SHOW_DATA';

// show related actions
export const SUBSCRIBE = 'SUBSCRIBE';
export const UNSUBSCRIBE = 'UNSUBSCRIBE';

// player related actions
export const SET_STREAM = 'SET_STREAM';
export const TOGGLE_PLAY = 'TOGGLE_PLAY';
export const STOP = 'STOP_PLAYER';
export const BUFFERING = 'BUFFERING';

// request related actions
export const FETCH_FEATURED_SHOWS = 'FETCH_FEATURED_SHOWS';
export const FETCH_FEATURED_SHOWS_SUCCESS = 'FETCH_FEATURED_SHOWS_SUCCESS';
export const FETCH_FEATURED_SHOWS_FAILURE = 'FETCH_FEATURED_SHOWS_FAILURE';

export const FETCH_SHOW = 'FETCH_SHOW';
export const FETCH_SHOW_SUCCESS = 'FETCH_SHOW_SUCCESS';
export const FETCH_SHOW_FAILURE = 'FETCH_SHOW_FAILURE';

export const FETCH_STATIONS = 'FETCH_STATIONS';
export const FETCH_STATIONS_SUCCESS = 'FETCH_STATIONS_SUCCESS';
export const FETCH_STATIONS_FAILURE = 'FETCH_STATIONS_FAILURE';


// ui related actions
export const TOGGLE_LIVE_MENU = 'TOGGLE_LIVE';
export const TOGGLE_NAV_MENU = 'TOGGLE_NAV';
export const TOGGLE_SEARCH = 'TOGGLE_SEARCH';


// notification actinos
export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';
