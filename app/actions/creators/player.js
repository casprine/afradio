import { SET_STREAM, TOGGLE_PLAY, STOP, BUFFERING } from 'actions';

/**
 * Returns a payload for triggering a
 * set stream action
 *
 * @param string stream
 * @param string poster
 * @param string title
 * @param string subtext
 *
 * @returns object
 */
export function setStream(stream, poster, title, subtext) {
  return {
    type: SET_STREAM,
    payload: {
      open: true,
      playing:true,
      stream: {
        current: stream,
      },
      image: poster,
      title,
      subtext,
    },
  };
}

/**
 * Returns a payload for triggering a
 * toggle play action
 *
 * @param {boolean} playing
 * @returns object
 */
export function togglePlay(playing) {
  return {
    type: TOGGLE_PLAY,
    payload: playing
  };
}

/**
 * Returns a payload for triggering a
 * buffering action
 *
 * @returns object
 */
export function buffering(isBuffering) {
  return {
    type: BUFFERING,
    payload: {
        buffering:isBuffering
    }
  };
}

/**
 * Returns a payload for triggering a
 * stop action on the player
 *
 * @returns object
 */
export function stop() {
  return {
    type: STOP,
    payload: {
      open: false,
      playing: false,
      buffering:false,
      stream: {
        current: null,
        prev: null,
      },
    },
  };
}
