import {
  FETCH_FEATURED_SHOWS,
  FETCH_FEATURED_SHOWS_SUCCESS,
  FETCH_FEATURED_SHOWS_FAILURE,
  FETCH_SHOW,
  FETCH_SHOW_SUCCESS,
  FETCH_SHOW_FAILURE,
  FETCH_STATIONS,
  FETCH_STATIONS_SUCCESS,
  FETCH_STATIONS_FAILURE
} from 'actions';

export function fetchFeatured(){
    return {
        type : FETCH_FEATURED_SHOWS
    };
}

export function fetchFeaturedSuccess(){
    return {
        type : FETCH_FEATURED_SHOWS_SUCCESS
    };    
}

export function fetchFeaturedFailed(error){
    return {
        type : FETCH_FEATURED_SHOWS_FAILURE,
        payload : error
    };
}

export function fetchStations(){
    return {
        type : FETCH_STATIONS
    };
}

export function fetchStationsSuccess(){
    return {
        type : FETCH_STATIONS_SUCCESS
    };    
}

export function fetchStationsFailed(error){
    return {
        type : FETCH_STATIONS_FAILURE,
        payload : error
    };    
}

export function fetchShow(id){
    return {
        type : FETCH_SHOW,
        payload : id
    };    
}

export function fetchShowFailed(id,error){
    return {
        type : FETCH_SHOW_FAILURE,
        payload : { id, error }
    };    
}

export function fetchShowSuccess(id){
    return {
        type : FETCH_SHOW_SUCCESS,
        payload : id
    };    
}