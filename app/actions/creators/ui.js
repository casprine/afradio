import { TOGGLE_LIVE_MENU, TOGGLE_NAV_MENU,TOGGLE_SEARCH } from 'actions';

/**
 * Returns a payload for toggling the live menu
 *
 * @returns {undefined}
 */
export function toggleLiveMenu(open) {
  return {
    type: TOGGLE_LIVE_MENU,
    payload: {
      liveMenu: open,
      nav: false,
    },
  };
}

/**
 * Returns a payload for toggling the nav menu
 *
 * @returns {undefined}
 */
export function toggleNavMenu(open) {
  return {
    type: TOGGLE_NAV_MENU,
    payload: {
      liveMenu: false,
      nav: open,
    },
  };
}

/**
 * Returns a payload for toggling the nav menu
 *
 * @returns {undefined}
 */
export function toggleSearch(open) {
  return {
    type: TOGGLE_SEARCH,
    payload: {
      search:open
    },
  };
}
