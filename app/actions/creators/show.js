import { SUBSCRIBE, UNSUBSCRIBE } from 'actions';

export function subscribe(id) {
  return {
    type: SUBSCRIBE,
    payload: {
      id,
    },
  };
}

export function unsubscribe(id) {
  return {
    type: UNSUBSCRIBE,
    payload: {
      id,
    },
  };
}

