import {
    SET_SHOWS, 
    SET_STATIONS, 
    UPDATE_LIVE_SHOWS, 
    ADD_SHOW, 
    UPDATE_FEATURED_SHOWS,
    INITIALIZE_SHOW_DATA
} from 'actions';
import {
    fetchFeatured,
    fetchFeaturedSuccess,
    fetchFeaturedFailed,
    fetchStations as fetchStationsRequest,
    fetchStationsSuccess,
    fetchStationsFailed,
    fetchShow as fetchShowRequest,
    fetchShowSuccess,
    fetchShowFailed
} from 'actions/creators/request';
import {
    fetchShows, 
    fetchStations, 
    fetchCategories, 
    fetchShow
} from 'lib/afradio_api';
import { 
    filterLiveShows,
    mapArrayToObject,
    flattenShows,
    flattenStation,
    timeToFifthMinute 
} from 'lib/util';

export function loadShows() {
    return dispatch => {
        
        dispatch(fetchFeatured());
        fetchShows('GH', 'NG', 'KE')
        .end((err, res) => {
            if (err) {
                dispatch(fetchFeaturedFailed("an error occured"));
            }
            const normalizedShows = 
                mapArrayToObject(res.body,'_id',flattenStation); 
                        
            dispatch(initializeShowData(
                normalizedShows,
                filterLiveShows(normalizedShows),
                Object.keys(normalizedShows)
            ));
            dispatch(fetchFeaturedSuccess());
            dispatch(scheduleLiveShowRefresh());
        });
    };
}

export function loadStations() {
    return dispatch => {
        
        dispatch(fetchStationsRequest())
        fetchStations('GH', 'NG', 'KE')
        .end((err, res) => {
            if (err) {
                dispatch(fetchStationsFailed("an error occured"));
            }
            const normalizedStations = 
                mapArrayToObject(res.body,'_id',flattenShows); 
            
            dispatch(setStations(normalizedStations));
            dispatch(fetchStationsSuccess());
        });

    };
}

export function loadShow(id) {
    return dispatch => {
       
       dispatch(fetchShowRequest(id));
       fetchShow(id)
       .end((err,res) => {
            if (err) {
               dispatch(fetchShowFailed(id,"an error ocurred"));
            }
            const normalizedShow = 
                mapArrayToObject([res.body],'_id',flattenStation); 
            dispatch(addShow(normalizedShow));
            dispatch(fetchShowSuccess(id));
       });
    }
}

export function loadCategories() {
    return dispatch => {

        fetchCategories('GH', 'NG', 'KE')
        .end((err, res) => {
            if (err) {
                console.log('an error ocurred');
            }

            dispatch(setStations(res.body));
        });

    };
}

export function scheduleLiveShowRefresh(){
    return (dispatch,getState) => {
        const {shows} = getState().appState;
        setTimeout(() => {
            dispatch(updateLiveShows(filterLiveShows(shows)));
            dispatch(scheduleLiveShowRefresh());
        }, timeToFifthMinute());
    };
}

export function initializeShowData(shows,live,featured){
    return {
        type:INITIALIZE_SHOW_DATA,
        payload:{
            shows:shows,
            live:live,
            featured:featured
        }
    }
}

export function updateLiveShows(liveShows) {
    return {
        type: UPDATE_LIVE_SHOWS,
        payload: {
          live: liveShows
        }
      };
}

function updateFeaturedShows(featuredShows){
    return {
        type: UPDATE_FEATURED_SHOWS,
        payload:{
            featured:featuredShows
        }
    };
}

export function setShows(shows) {
    return {
        type: SET_SHOWS,
        payload: { shows }
    };
}

export function addShow(show) {
    return {
        type: ADD_SHOW,
        payload: { show }
    };
}

export function setStations(stations) {
    return {
      type: SET_STATIONS,
      payload: { stations }
    };
}