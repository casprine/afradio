import {ADD_NOTIFICATION,REMOVE_NOTIFICATION} from 'actions';

export function notify(notification){
    return {
        type : ADD_NOTIFICATION,
        payload : {
            ...notification,
            id:new Date().getTime(),
            type:notification.type || 'default'
        }
    };
}

export function removeNotification(id){
    return {
        type : REMOVE_NOTIFICATION,
        payload : id
    };
}

