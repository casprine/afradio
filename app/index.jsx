import React from 'react';
import ReactDOM from 'react-dom';
import routes from 'routes';
import { Provider } from 'react-redux';
import store from 'store';
import { initialize as initNotificationScheduler } from 'lib/notificationScheduler';
//import { HashRouter as Router } from 'react-router-dom';
import createHistory from 'history/createHashHistory';
import { ConnectedRouter,push } from 'react-router-redux';
import {retrieveObject} from 'lib/storage';
import {computeNotificationDelay,millisecondsUntilMidnight} from 'lib/util';
import {schedule} from 'lib/notificationScheduler';

const history = createHistory();
const queueNotifications = () => {
    let subscriptions = retrieveObject('subscriptions');
    for(let showId in subscriptions){
        let 
            {title,icon,schedules} = subscriptions[showId],
            notificationDelay = computeNotificationDelay(schedules);
        if(notificationDelay > -1){
            schedule({title:`${title} is live`,icon,id:showId},notificationDelay);
        }
    }
    
    setTimeout(queueNotifications,millisecondsUntilMidnight());
};

function AppRoot(props){
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
            {routes}
        </ConnectedRouter>
      </Provider>
    );
}

ReactDOM.render(<AppRoot/>, document.getElementById('app'));
initNotificationScheduler();
queueNotifications();
