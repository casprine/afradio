import {ADD_NOTIFICATION,REMOVE_NOTIFICATION} from 'actions';

const initialState = [];

export default (state=initialState,action) => {
    switch(action.type){
        case ADD_NOTIFICATION:  
            return [...state,action.payload];
        case REMOVE_NOTIFICATION:
            return state.filter( x => x.id !== action.payload);
        default:
            return state;
    }
};