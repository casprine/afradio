import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import player from './player';
import ui from './ui';
import appState from './app';
import notifications from './notifications';
import request from './request';


export default combineReducers({
  routing, appState, player, ui ,request ,notifications
});