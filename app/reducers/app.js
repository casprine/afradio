import { 
    SET_SHOWS, 
    SET_STATIONS,
    ADD_SHOW,
    UPDATE_LIVE_SHOWS,
    UPDATE_FEATURED_SHOWS,
    INITIALIZE_SHOW_DATA,
    SUBSCRIBE,
    UNSUBSCRIBE
} from 'actions'; 
import { retrieveObject,remove } from 'lib/storage';


function getSubscriptions() {
  let subs = retrieveObject('subscriptions');
  if(Array.isArray(subs)){
    remove('subscriptions');
    subs = {}; 
  }
  return Object.keys(subs);
}

const initialState = {
  built: true,
  searching: false,
  stations: {},
  shows: {},
  live: [],
  featured:[],
  subscriptions: getSubscriptions()
};

export default (state = initialState, action) => {
  switch (action.type) {
    case INITIALIZE_SHOW_DATA:
        return {
            ...state,
            shows:{...state.shows,...action.payload.shows},
            live:action.payload.live,
            featured:action.payload.featured
        }; 
    case SET_STATIONS:
      const newState = Object.assign({}, state, { stations: action.payload.stations });
      return newState;
    case SET_SHOWS:
      return Object.assign({}, state, { shows: {...state.shows,...action.payload.shows}});
    case ADD_SHOW:
        return Object.assign({}, state, {shows: {...state.shows,...action.payload.show}});
    case UPDATE_LIVE_SHOWS:
      return Object.assign({}, state, { live: action.payload.live });
    case UPDATE_FEATURED_SHOWS:
        return Object.assign({}, state, { featured: action.payload.featured });
    case SUBSCRIBE:
      return {...state, subscriptions:[...state.subscriptions,action.payload.id]};
    case UNSUBSCRIBE:
      return {...state, subscriptions: state.subscriptions.filter(x => x!== action.payload.id)};
    default:
      return state;
  }
}