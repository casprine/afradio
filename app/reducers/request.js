import {
FETCH_FEATURED_SHOWS,
FETCH_FEATURED_SHOWS_SUCCESS,
FETCH_FEATURED_SHOWS_FAILURE,
FETCH_SHOW,
FETCH_SHOW_SUCCESS,
FETCH_SHOW_FAILURE,
FETCH_STATIONS,
FETCH_STATIONS_SUCCESS,
FETCH_STATIONS_FAILURE
} from "actions";
import {REQUEST_STATES} from 'lib/util';

let initialState = {
    shows: {
        pending: [],
        errors: {}
    },
    featured: {
        shows: {
            status: REQUEST_STATES.PENDING,
            error: null
        },
        stations: {
            status: REQUEST_STATES.PENDING,
            error: null
        }
    }
};

export default (state = initialState, action) => {
    let
    featuredRequestState,
    showRequestState;
    
    switch (action.type) {
        case FETCH_FEATURED_SHOWS:
            featuredRequestState = {
                ...state.featured,
                shows: {status: REQUEST_STATES.PENDING}
            };
            return ({
                ...state, featured: featuredRequestState
            });
        case FETCH_FEATURED_SHOWS_SUCCESS:
            featuredRequestState = {
                ...state.featured,
                shows: {status: REQUEST_STATES.COMPLETE}
            };
            return {...state,featured: featuredRequestState};
            
        case FETCH_FEATURED_SHOWS_FAILURE:
            featuredRequestState = {
                ...state.featured,
                shows: {status: REQUEST_STATES.FAILED, error: action.payload}                        
            };
            return {...state, featured: featuredRequestState };
        case FETCH_SHOW:
            let {[action.payload]:current,...otherErrors} = state.shows.errors;
            
            showRequestState = {
                ...state.shows,
                pending : [...state.shows.pending, action.payload],
                errors : otherErrors
            };
            return {...state,shows:showRequestState}
        case FETCH_SHOW_SUCCESS:
            showRequestState = {
                ...state.shows,
                pending : state.shows.pending.filter(x => x !== action.payload)
            };
            return {...state,shows:showRequestState};
        case FETCH_SHOW_FAILURE:
            showRequestState = {
                ...state.shows,
                pending : state.shows.pending.filter(x => x !== action.payload.id),
                errors : {
                    ...state.shows.errors,
                    [action.payload.id] : action.payload.error
                }
            }
            return {...state,shows:showRequestState}
        case FETCH_STATIONS:
            featuredRequestState = {
                ...state.featured,
                stations: {status: REQUEST_STATES.PENDING},
            };
            return {...state, featured: featuredRequestState};

        case FETCH_STATIONS_SUCCESS:
            featuredRequestState = {
                ...state.featured,
                stations: {status: REQUEST_STATES.COMPLETE}
            };
            return {...state, featured: featuredRequestState};

        case FETCH_STATIONS_FAILURE:
            featuredRequestState = {
                ...state.featured,
                stations: {status: REQUEST_STATES.FAILED, error: action.payload}
            };
            return {...state, featured: featuredRequestState};
        default :
            return state;
    }
};