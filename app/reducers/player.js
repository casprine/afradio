import { STOP, TOGGLE_PLAY, SET_STREAM, BUFFERING } from 'actions';

/**
 * The initial state of the player component
 *
 * @type object
 */
const initialState = {
  open: false,
  playing: false,
  buffering:false,
  stream: {
    current: null,
    prev: null,
  },
  image: null,
  title: null,
  subtext: null,
  live: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case BUFFERING:
        return {...state,...action.payload};
    case STOP:
      return Object.assign({}, state, action.payload);
    case SET_STREAM:
      action.payload.stream.prev = state.stream.current;
      return Object.assign({}, state, action.payload);
    case TOGGLE_PLAY:
      if(state.buffering === true) return state;
      state.stream.prev = state.stream.current;
      return Object.assign({}, state, { playing: action.payload });
    default:
      return state;
  }
};

