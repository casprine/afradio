import { TOGGLE_LIVE_MENU, TOGGLE_NAV_MENU, TOGGLE_SEARCH } from 'actions';

/**
 * The initial state of the player component
 *
 * @type object
 */
const initialState = {
  liveMenu: false,
  nav: false,
  search:false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_LIVE_MENU:
      return Object.assign({}, state, action.payload);
    case TOGGLE_NAV_MENU:
      return Object.assign({}, state, action.payload);
    case TOGGLE_SEARCH:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};

