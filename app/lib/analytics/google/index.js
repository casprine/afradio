const shareMediums = {
    facebook: "Facebook",
    twitter: "Twitter",
    googlePlus: "Google Plus",
    linkedin: "LinkedIn"
}

function record(details) {
    console.log(details);
    //ga('send', details);
}

export function playbackPlayed(id, title) {
    let details = {
        hitType: 'event',
        eventCategory: 'playback',
        eventAction: 'played',
        eventLabel: `Playback started for playback with id: ${id}`,
        show: title,
        playbackId: id
    };

    record(details);
}

export function playbackPaused(id, title) {
    let details = {
        hitType: 'event',
        eventCategory: 'playback',
        eventAction: 'paused',
        eventLabel: `Playback paused for playback with id: ${id}`,
        show: title,
        playbackId: id
    };

    record(details);
}

export function playbackStaged(id, title) {
    let details = {
        hitType: 'event',
        eventCategory: 'playback',
        eventAction: 'staged',
        eventLabel: `Playback staged for playback with id: ${id}`,
        show: title,
        playbackId: id
    };

    record(details);
}

export function searchPerformed(term) {
    let details = {
        hitType: 'event',
        eventCategory: 'search',
        eventAction: 'search',
        eventLabel: `Searched for: ${term}`
    };

    record(details);
}

/**
 * Records a share event on a playback item
 * 
 * @param {String} id
 * @param {String} url
 * @param {String} medium
 * @returns {undefined}
 */
export function playbackShared(id, url, medium) {
    let details = {
        hitType: 'social',
        socialNetwork: this.shareMediums[medium],
        socialAction: 'share',
        socialTarget: url,
        playbackId: id
    };

    record(details);
}

export function setupFailed(){
    let 
        details = {
            hitType: 'event',
            eventCategory: 'error',
            eventAction: 'API Request',
            nonInteraction: true,
            eventLabel: 'API didn\'t respond in time'
        };
    
    record(details);
}