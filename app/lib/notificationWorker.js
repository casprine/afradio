const notificationHandler = {
    pending: {},
    add(notification, timeout) {
        let {title,icon,id} = notification;
        
        this.pending[id] = setTimeout(() => {
           new Notification(title,{icon}); 
        },timeout);
    },
    cancel(id) {
        if (!this.pending[id])
            return;
        clearTimeout(this.pending[id]);
        delete this.pending[id];
    }
};

onmessage = function (e) {
    let {method, args} = e.data;
    notificationHandler[method](...args);
};

