import {stripQueryString,buildQueryString} from 'lib/util';

const SHARING_PLATFORMS = {
    TWITTER : "twitter",
    FACEBOOK : "facebook",
    GOOGLEPLUS : "google-plus",
    LINKEDIN : "linkedin"
};

export function share(platform, params,callback) {
    switch(platform){
        case SHARING_PLATFORMS.TWITTER:
            twitterShare(params);
            break;
        case SHARING_PLATFORMS.FACEBOOK:
            facebookShare(params);
            break;
        case SHARING_PLATFORMS.GOOGLEPLUS:
            googlePlusShare(params);
            break;
        case SHARING_PLATFORMS.LINKEDIN:
            linkedinShare(params);
            break;
    }
    
    if(!!callback) callback();
}

function twitterShare(params) {
    
    let twParams = {
        url: params.url,
        text: params.title + encodeURIComponent(" #AFRadio")
    };
    openPopup(
        'https://twitter.com/intent/tweet'
        , twParams
        , 'twitter-popup'
        );
}

function facebookShare(params) {
    let fbParams = {url: params.url, title: params.title};
    openPopup(
        'https://www.facebook.com/sharer/sharer.php'
        , fbParams
        , 'facebook-popup'
        );
}

function googlePlusShare(params) {
    let gpParams = {url: params.url, prefilltext: params.title};
    openPopup(
        'https://plus.google.com/share'
        , gpParams
        , 'google-plus-popup'
        );
}

function linkedinShare(params) {
    let lnParams = {
        url: params.url,
        mini: true,
        prefilltext: params.title,
        summary: params.title
    };
    openPopup(
        'https://www.linkedin.com/shareArticle'
        , lnParams
        , 'linkedin-popup'
        );
}

function shortenUrl(url){
    return new Promise((resolve,reject) => {        
        let 
            http = new XMLHttpRequest(),
            endpoint = "https://api.afrad.io/api/url/shorten",
            params = `url=${url}`;

        http.open("POST", endpoint, true);
        http.setRequestHeader(
            "Content-type", "application/x-www-form-urlencoded"
        );

        http.onreadystatechange = function() {
            if(http.readyState !== XMLHttpRequest.DONE) return; 
            if(http.status !== 200){
                reject(url);
            }

            let response =  JSON.parse(http.responseText);
            if(response.msg && response.msg === "invalid url"){
                reject(url);
            }
            resolve(response.url);        
        };
        http.send(params);
    });
}

export function constructShareUrl(domain, params) {
    let
        plainUrl = stripQueryString(domain),
        queryString = buildQueryString(params);
    return `${plainUrl}?${queryString}`;
}

function openPopup(url, params, title) {
    const share = shortenedUrl => {
        params.url = encodeURIComponent(shortenedUrl); 
        let shareWindow = window.open(
        `${url}?${buildQueryString(params)}`
        , title
        , 'height=350,width=600'
        );
        if (shareWindow.focus) {
            shareWindow.focus();
        }
    };
    shortenUrl(params.url).then(share)
    .catch(share);    
}

export {SHARING_PLATFORMS};