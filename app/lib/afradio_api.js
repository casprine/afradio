import request from 'superagent';

//const API_ENDPOINT = 'https://app.afradio.co/api';
const API_ENDPOINT = 'https://api.afrad.io/api';

const AFDAYS_AS_STRINGS = new Map([
  [1, 'Sunday'],
  [2, 'Monday'],
  [3, 'Tuesday'],
  [4, 'Wednesday'],
  [5, 'Thursday'],
  [6, 'Friday'],
  [7, 'Saturday'],
]);

/**
 *
 * @param {type} countries
 * @returns {unresolved}
 */
export function fetchShows(...countries) {
  countries = countries.join(',');
  return request.get(
    `${API_ENDPOINT}/shows?country=${countries}`
  );
}

/**
 *
 * @param {type} countries
 * @returns {unresolved}
 */
export function fetchShow(id) {
  return request.get(
    `${API_ENDPOINT}/shows/${id}`
  );
}

/**
 *
 * @param {type} countries
 * @returns {unresolved}
 */
export function fetchStations(...countries) {
  countries = countries.join(',');
  return request.get(
    `${API_ENDPOINT}/stations?country=${countries}`
  );
}

/**
 *
 * @param {type} countries
 * @returns {unresolved}
 */
export function fetchShowsByCategory(category, ...countries) {
  countries = countries.join(',');
  return request.get(
    `${API_ENDPOINT}/categories/${category}?country=${countries}`
  );
}

/**
 *
 * @returns {unresolved}
 */
export function fetchCategories() {
  return request.get(`${API_ENDPOINT}/categories`);
}

/**
 *
 * @param {type} showId
 * @returns {unresolved}
 */
export function subscribe(showId) {
  return request.get(`${API_ENDPOINT}/shows/${showId}/subscribe`);
}

/**
 *
 * @param {type} show
 * @returns {unresolved}
 */
export function fetchPlaybacksForShow(show) {
  return request.get(`${API_ENDPOINT}/shows/${show}/playback`);
}