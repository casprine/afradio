/**
 * Module that abstracts the window.localStorage API
 * so that data storage operations can be handled cleanly
 *
 * @type module
 */

/**
 *
 * @param {type} key
 * @param {type} fallBack
 * @returns {Array|Object|undefined}
 */
export function retrieve(key, fallBack) {
  const item = window.localStorage.getItem(key);
  return item ? JSON.parse(item) : (fallBack || undefined);
}

/**
 *
 * @param {type} key
 * @returns {Array|type|Object|undefined}
 */
export function retrieveArray(key) {
  return retrieve(key, []);
}

/**
 *
 * @param {type} key
 * @returns {Array|type|Object|undefined}
 */
export function retrieveObject(key) {
  return retrieve(key, {});
}

/**
 *
 * @param {type} key
 * @param {type} data
 * @returns {undefined}
 */
export function persist(key, data) {
  window.localStorage.setItem(key, JSON.stringify(data));
}

/**
 *
 * @param {type} key
 * @returns {unresolved}
 */
export function remove(key) {
  return window.localStorage.removeItem(key);
}

/**
 *
 * @param {type} id
 * @returns {undefined}
 */
export function addSubscription(id,meta) {
  let subscriptions = retrieveObject('subscriptions');
  subscriptions[id] = meta;
  persist('subscriptions', subscriptions);
}

/**
 *
 * @param {type} id
 * @returns {undefined}
 */
export function removeSubscription(id) {
  let subscriptions = retrieveObject('subscriptions');
  if(subscriptions[id]){
      delete subscriptions[id];
  }
  persist('subscriptions', subscriptions);
}
