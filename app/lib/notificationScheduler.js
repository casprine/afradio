let notificationWorker = null;

export function initialize(){
    window.onload = () => {
       if("Notification" in window){ 
            if( Notification.permission === "default" || 
                Notification.permission === "denied") {
                Notification.requestPermission();
            }
        }
    };
    
    if(window.Worker){
        notificationWorker = new Worker('build/notificationWorker.js');
    }    
}

function invokeWorkerMethod(method,...args){
    notificationWorker.postMessage({method,args});
}

export function schedule(notification,timeout){
  invokeWorkerMethod('add',notification,timeout);  
}

export function unschedule(id){
   invokeWorkerMethod('cancel',id);   
}

export { notificationWorker };