export const REQUEST_STATES = {
    PENDING : "PENDING",
    COMPLETE : "COMPLETE",
    FAILED : "FAILED"
};

/**
 *
 * @param {type} subject
 * @param {type} pad
 * @param {type} length
 * @returns {String}
 */
export function padString(subject, pad, length) {
  let padding = '',
    x = 0;
  while (x < length) {
    padding += pad;
    x++;
  }
  return (padding + subject).slice(-length);
}


/**
 *
 * @param {type} shows
 * @returns {unresolved}
 */
export function filterLiveShows(shows) {
    const liveShows = [];
    
    for(let show in shows){
        if(showIsLive(shows[show].schedules)){
           liveShows.push(show); 
        }
    }
    
    return liveShows;
}

/**
 *
 * @param {type} category
 * @param {type} shows
 * @returns {unresolved}
 */
export function filterByCategory(category, shows) {
  if(!category){
      return shows;
  }
  
  const filteredShows = {};
  for(let showId in shows){
      let show = shows[showId];
      if(show.category && show.category.key === category){
          filteredShows[showId] = show;
      }
  }
  
  return filteredShows;
}

/**
 *
 * @param {type} category
 * @param {type} shows
 * @returns {unresolved}
 */
export function filterByStation(station, shows) {
  if(!station){
      return shows;
  }
  
  const filteredShows = {};
  for(let showId in shows){
      let show = shows[showId];
      if(show.category && show.category.key === station){
          filteredShows[showId] = show;
      }
  }
  
  return filteredShows;
}

/**
 *
 * @param {type} dateString
 * @returns {parseDate.date|Date}
 */
export function parseDate(dateString) {
  let
    dateTimeParts = dateString.split('T'),
    dateParts = dateTimeParts[0].split('-'),
    timeParts = dateTimeParts[1].split(':').splice(0, 2),
    date = new Date();

  date.setFullYear(Number.parseInt(dateParts[0]));
  date.setMonth(Number.parseInt(dateParts[1]) - 1);
  date.setDate(Number.parseInt(dateParts[2]));
  date.setHours(Number.parseInt(timeParts[0]));
  date.setMinutes(Number.parseInt(timeParts[1]));
  return date;
}

/**
 * Converts the time in minutes to a time string
 *
 * @param {type} minutes
 * @returns {undefined}
 */
export function minutesToTimeString(minutes) {
  let suffix = 'am';
  let hours = Math.floor(minutes / 60);
  const mins = minutes % 60;
  if (hours > 12) {
    hours = Math.abs(12 - hours);
    suffix = 'pm';
  }

  return `${padString(hours, '0', 2)} : ${padString(mins, '0', 2)} ${suffix}`;
}

/**
 * Returns the string representation of a day with respect
 * to the afradio spec
 *
 * @param {Number} dayAsInteger
 * @returns {string}
 */
export function afDayAsString(dayAsInteger) {
  return AFDAYS_AS_STRINGS.get(dayAsInteger);
}

/**
 *
 * @param {type} time
 * @param {type} start
 * @param {type} end
 * @returns {Boolean}
 */
function isWithinTimeRange(time, start, end) {
  return ((time >= start) && (time < end));
}

/**
 *
 * @param {type} val
 * @returns {Boolean}
 */
function isObject(val) {
  if (val === null) {
    return false;
  }
  return ((typeof val === 'function') || (typeof val === 'object'));
}

export function mapArrayToObject(arrayToMap,keyProperty,cb){
    const outputObject = {};
    
    arrayToMap.forEach(item => {
        if(cb) item = cb(item);
        outputObject[item[keyProperty]] = item;
    });
    
    return outputObject;
}

export function flattenShows(station){
    if(!!!station.shows) return station;
    station.shows = station.shows.map( show => show._id);
    return station;
}

export function flattenStation(show){
    if(!!!show.station) return show;
    let station = show.station;
    show.stationId = station._id;
    show.streamUrl = station.streamUrl;
    show.station = station.name;
    return show;
}

export function millisecondsUntilMidnight() {
    var midnight = new Date();
    midnight.setHours( 24 );
    midnight.setMinutes( 0 );
    midnight.setSeconds( 0 );
    midnight.setMilliseconds( 0 );
    return ( midnight.getTime() - new Date().getTime() );
}

export function showAirsToday(schedules){
 let
    date = new Date(),
    dayOfWeek = date.getDay() + 1,
    eligibleSchedules = schedules.filter(schedule => schedule.day === dayOfWeek);
    return eligibleSchedules.length > 0;
}

export function timeTillShow(schedules){
 let
    date = new Date(),
    dayOfWeek = date.getDay() + 1,
    eligibleSchedules = schedules.filter(x => x.day === dayOfWeek),
    timeInMinutes = (date.getHours() * 60) + date.getMinutes(); 
    if(eligibleSchedules.length > 0){
        let eligibleSchedule = eligibleSchedules.shift();
        if(eligibleSchedule.start < timeInMinutes) return -1;
        return eligibleSchedule.start - timeInMinutes;
    }
    
    return -1;
}

export function stripQueryString(url) {
    url = url.split("?")[0];
    url = url.split("%3F")[0];
    return url;
}

export function buildQueryString(params) {
    let queryString = "";

    for (let prop in params) {
        if (params.hasOwnProperty(prop)) {
            queryString +=
                "&" + `${prop}=${params[`${prop}`]}`;
        }
    }

    return queryString.substring(1);
}

export function computeNotificationDelay(schedules){
    let 
        airsToday = showAirsToday(schedules),
        isLive = showIsLive(schedules);
    if(isLive){
        return 0;
    }
    
    return (timeTillShow(schedules) * 60000);
}

/**
 *
 * @param {type} show
 * @returns {live|Boolean}
 */
function showIsLive(schedules) {
  let
    date = new Date(),
    dayOfWeek = date.getDay() + 1,
    eligibleSchedules = schedules.filter(x => x.day === dayOfWeek),
    timeInMinutes = (date.getHours() * 60) + date.getMinutes();

  if (eligibleSchedules.length > 0) {
    let live = false;
    eligibleSchedules.forEach((schedule) => {
      live =
                live ||
                isWithinTimeRange(
                  timeInMinutes,
                  schedule.start,
                  (schedule.start + schedule.duration),
                );
    });
    return live;
  }
  return false;
}

export function timeToFifthMinute(){
    const 
        x = new Date().getMinutes() % 10,
        minutesToFifth = (x >= 5) ? 10 - x : 5 - x;
    
    return 60000 * minutesToFifth;    
}


export function notifySuccess(notificationContent){
    return {
        message: notificationContent,
        type:'success',
        delay:2500
    };
}

export function notifyFailure(notificationContent){
    return {
        message: notificationContent,
        type:'failed',
        delay:2500
    };
}