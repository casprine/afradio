let gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  webpackStream = require('webpack-stream'),
  webpack = require('webpack'),
  browserSync = require('browser-sync').create(),
  uglify = require('gulp-uglify'),
  path = require('path'),
  util = require('gulp-util'),
  eslint = require('gulp-eslint'),
  ifFixed = require('gulp-eslint-if-fixed'),
  plumber = require('gulp-plumber'),
  isProduction = util.env.production,
  watch = require('gulp-watch'),
  named = require('vinyl-named'),
  io = {
    assets: path.resolve(`${__dirname}/assets/`),
    app: path.resolve(`${__dirname}/app/`),
    dest: path.resolve(`${__dirname}/build/`),
    prodDest: path.resolve(`${__dirname}/dist/build/`),
  },
  cssPaths = {
    input: `${io.assets}/css/main.scss`,
    output: isProduction ? `${io.prodDest}/css` : `${io.dest}/css/`,
    watch: `${io.assets}/css/**/*.*`,
  },
  vendorCssPaths = {
    input: `${io.assets}/css/*.css`,
    output: cssPaths.output,
  },
  imagePaths = {
    input: `${io.assets}/images/*.*`,
    output: isProduction ? `${io.prodDest}/images/` : `${io.dest}/images/`,
  },
  fontPaths = {
    input: `${io.assets}/fonts/**/*.*`,
    output: isProduction ? `${io.prodDest}/fonts/` : `${io.dest}/fonts/`,
  },
  jsPaths = {
    input: [`${io.app}/index.jsx`, `${io.app}/lib/notificationWorker.js`],
    watch: [`${io.app}/**/*.js`, `${io.app}/**/*.jsx`],
    output: isProduction ? io.prodDest : io.dest,
  };

gulp.task('eslint', (done) => {
  gulp
    .src('./app/**/*.js?(x)', {
      since: gulp.lastRun('eslint'),
    })
    .pipe(plumber())
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(ifFixed(io.app))
    .pipe(eslint.result((result) => {
      console.log(`ESLint result: ${result.filePath}`);
      console.log(`# Messages: ${result.messages.length}`);
      console.log(`# Warnings: ${result.warningCount}`);
      console.log(`# Errors: ${result.errorCount}`);
    }));
  done();
});

gulp.task('sass', () =>
  gulp
    .src(cssPaths.input)
    .pipe(plumber())
    .pipe(sass({
      outputStyle: 'compressed',
    }))
    .pipe(autoprefixer())
    .pipe(gulp.dest(cssPaths.output))
    .pipe(browserSync.stream()));

gulp.task('copyImages', () => gulp.src(imagePaths.input).pipe(gulp.dest(imagePaths.output)));

gulp.task('copyVendorCss', () =>
  gulp.src(vendorCssPaths.input).pipe(gulp.dest(vendorCssPaths.output)));

gulp.task('copyFonts', () => gulp.src(fontPaths.input).pipe(gulp.dest(fontPaths.output)));

gulp.task(
  'copyAssets',
  gulp.series(gulp.parallel('copyImages', 'copyFonts', 'copyVendorCss'), (done) => {
    done();
  }),
);

gulp.task(
  'bundle',
  gulp.series(
    /* 'eslint', */ () =>
      gulp
        .src(jsPaths.input)
        .pipe(plumber())
        .pipe(named())
        .pipe(webpackStream(require('./webpack.config.js'), webpack))
        .pipe(uglify())
        .pipe(gulp.dest(jsPaths.output))),
);

gulp.task('serve', (done) => {
  browserSync.init({
    server: {
      baseDir: './',
    },
  });
  done();
});

gulp.task(
  'watch:bundle',
  gulp.series('bundle', (done) => {
    browserSync.reload();
    done();
  }),
);

gulp.task(
  'default',
  gulp.series(gulp.parallel('sass', 'bundle', 'copyAssets'), (done) => {
    if (isProduction) {
      gulp
        .src(`${path.resolve(__dirname)}/index.html`)
        .pipe(gulp.dest(`${path.resolve(__dirname)}/dist`));
    }
    done();
  }),
);
gulp.task(
  'watch',
  gulp.series(gulp.parallel('sass', 'bundle', 'copyAssets', 'serve'), (done) => {
    watch(cssPaths.watch, gulp.series('sass'));
    watch(jsPaths.watch, gulp.series('watch:bundle'));
    gulp.watch('./*.html');
    done();
  }),
);
